jQuery(document).ready(function ($) {
  function fixedNavbar() {
    $('.navbar ').scrollToFixed();

  }

  function contentOffset() {
    var offsetContent = $('.footer .container').offset().left + 15
    $(".container--full").css({
      "padding-right": offsetContent,
    });
    $(".beneficiaries .container").css({
      "padding-left": offsetContent,
    });
    $(".founder__banner__caption").css({
      "padding-left": offsetContent,
      "padding-right": offsetContent
    });

  }

  fixedNavbar();
  contentOffset();
  $(window).resize(contentOffset);
  //   nav show and hide
  function navToggle() {
    $('body').on('click', '.navbar-toggler', function () {
      $(this).toggleClass('on');
      // $(this).parents('.menu-item-has-children').find('.sub-menu').slideToggle();
      $(this).toggleClass('open');
      $('.navbar').toggleClass('navbar--open');
    });
  }

  navToggle();

 function navDropdown () {
    $('ul.navbar-nav li.dropdown').hover(function () {
      $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(200);
    }, function () {
      $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(200);
    });
 }

//  navDropdown();

  $('.beneficiaries__slider').slick({
    dots: false,
    infinite: true,
    centerMode: true,
    centerPadding: '60px',
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 4,
    arrows: false,
    responsive: [{
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });

    function matchHeights() {
      var options = {
        byRow: false,
        byRow: true,
        property: 'height',
        target: null,
        remove: false
      };
      $('.latest__news--grid .item').matchHeight(options);

    }

    matchHeights();

  function gallerySlider() {
    if ($(window).width() < 991) {
      $(".gallery__slider").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        centerPadding: '90px',
        dots: false,
        infinite: true,
        centerMode: true,
      });


    } else {
      $(".gallery__slider").filter(".slick-initialized").slick("unslick");
    }
  }
  gallerySlider();
  $(window).resize(gallerySlider);

  function newsSlider() {
    if ($(window).width() < 991) {
      $(".latest__news--grid").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        centerPadding: '90px',
        dots: false,
        infinite: true,
        centerMode: true,
      });
      $('.center').on('init', function (event, slick, direction) {

      });


    } else {
      $(".latest__news--grid").filter(".slick-initialized").slick("unslick");
    }
  }
  newsSlider();
  $(window).resize(newsSlider);
});


jQuery(window).on("load", function () {
  var $ = jQuery
  $('body').addClass('loaded');
  setTimeout(function () {
    // $('.loader').fadeOut('1000');
  }, 1000);

});


